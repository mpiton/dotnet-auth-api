namespace api_rest
{
    public class FeedDto
    {
        public int CustomerId { get; set; } = 0;
        public string Link { get; set; } = string.Empty;
    }
}