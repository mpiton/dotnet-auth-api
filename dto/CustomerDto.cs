namespace api_rest
{
    public class CustomerDto
    {
        public int UserId { get; set; } = 0;
        public string StripeCustomer { get; set; } = string.Empty;
        public string StripeSubscription { get; set; } = string.Empty;
        public bool Active { get; set; } = false;
    }
}