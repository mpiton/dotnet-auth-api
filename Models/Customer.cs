using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace api_rest
{
    [Table("customers")]
    public class Customer
    {
        [Key, Column("id")]
        public int Id { get; set; }
        [Column("user_id")]
        public int UserId { get; set; } = 0;
        [Column("stripe_customer")]
        public string StripeCustomer { get; set; } = null!;
        [Column("stripe_subscription")]
        public string StripeSubscription { get; set; } = null!;
        [Column("active")]
        public bool Active { get; set; } = false;
    }

}