using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api_rest
{
    [Table("users")]
    public class User
    {
        [Key, Column("id")]
        public int Id { get; set; }
        [Column("email"), Required]
        public string Email { get; set; } = null!;
        [Column("password_hash"), Required]
        public byte[] PasswordHash { get; set; } = null!;
        [Column("password_salt"), Required]
        public byte[] PasswordSalt { get; set; } = null!;
        [Column("is_admin")]
        public bool IsAdmin { get; set; } = false;
        [Column("token")]
        public string Token { get; set; } = string.Empty;
        [Column("is_disabled")]
        public bool IsDisabled { get; set; } = false;
        [Column("created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}