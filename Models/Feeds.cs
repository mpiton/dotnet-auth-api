using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace api_rest
{
    [Table("feeds")]
    public class Feed
    {
        [Key, Column("id")]
        public int Id { get; set; }
        [Column("customer_id")]
        public int CustomerId { get; set; } = 0;
        [Column("link")]
        public string Link { get; set; } = null!;
        [Column("created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}