using api_rest.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api_rest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {
        public static Customer customer = new Customer();
        private readonly ApiDbContext _context;

        public CustomerController(ApiDbContext context)
        {
            _context = context;
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);
        }

        [HttpPost("create")]
        public IActionResult Create([FromBody] CustomerDto request)
        {
            // check if the user exists
            var user = _context.Users.FirstOrDefault(u => u.Id == request.UserId);
            if (user == null)
            {
                return BadRequest("User does not exist");
            }
            // check if the user is already a customer
            var customer = _context.Customers.FirstOrDefault(c => c.UserId == user.Id);
            Console.WriteLine(customer);
            if (customer != null)
            {
                return BadRequest("User is already a customer");
            }
            // create the customer
            customer = new Customer();
            customer.UserId = user.Id;
            customer.StripeCustomer = request.StripeCustomer;
            customer.StripeSubscription = request.StripeSubscription;
            customer.Active = true;
            // Save the customer in the database, if error return 500
            _context.Customers.Add(customer);
            _context.SaveChanges();

            return Ok(customer);
        }

        [HttpPost("update")]
        public IActionResult Update([FromBody] CustomerDto request)
        {
            // check if the user exists
            var user = _context.Users.FirstOrDefault(u => u.Id == request.UserId);
            if (user == null)
            {
                return BadRequest("User does not exist");
            }
            // check if the user is already a customer
            var customer = _context.Customers.FirstOrDefault(c => c.UserId == user.Id);
            if (customer == null)
            {
                return BadRequest("User is not a customer");
            }
            // update the customer
            customer.StripeCustomer = request.StripeCustomer;
            customer.StripeSubscription = request.StripeSubscription;
            customer.Active = request.Active;
            // Save the customer in the database, if error return 500
            _context.Customers.Update(customer);
            _context.SaveChanges();

            return Ok(customer);
        }

        [HttpGet("get/{userId}")]
        public IActionResult Get(int userId)
        {
            // check if the user exists
            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            if (user == null)
            {
                return BadRequest("User does not exist");
            }
            // check if the user is already a customer
            var customer = _context.Customers.FirstOrDefault(c => c.UserId == user.Id);
            if (customer == null)
            {
                return BadRequest("User is not a customer");
            }

            return Ok(customer);
        }

        [Authorize]
        [HttpGet("get-all")]
        public IActionResult GetAll()
        {
            var customers = _context.Customers.ToList();

            return Ok(customers);
        }
    }
}