using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using api_rest.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace api_rest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        public static User user = new User();
        private readonly IConfiguration _configuration;
        private readonly ApiDbContext _context;

        public AuthController(IConfiguration configuration, ApiDbContext context)
        {
            _configuration = configuration;
            _context = context;
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);
        }

        [HttpPost("register")]
        public Task<IActionResult> Register([FromBody] UserDto request)
        {
            if (user.Email == request.Email)
            {
                return Task.FromResult<IActionResult>(BadRequest("Email already exists"));
            }
            // check if the email is valid
            if (!new EmailAddressAttribute().IsValid(request.Email))
            {
                return Task.FromResult<IActionResult>(BadRequest("Invalid email"));
            }
            // Password validation
            if (string.IsNullOrEmpty(request.Password))
            {
                return Task.FromResult<IActionResult>(BadRequest("Password is required"));
            }
            if (request.Password.Length < 6)
            {
                return Task.FromResult<IActionResult>(BadRequest("Password must be at least 6 characters"));
            }
            CreatePasswordHash(request.Password, out var passwordHash, out var passwordSalt);

            user.Email = request.Email;
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            user.Token = string.Empty;

            // Save the user in the database, if error return 500
            _context.Users.Add(user);
            _context.SaveChanges();

            return Task.FromResult<IActionResult>(Ok(user));
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        [HttpPost("login")]
        public Task<IActionResult> Login([FromBody] UserDto request)
        {
            var user = _context.Users.FirstOrDefault(x => x.Email == request.Email);
            if (user == null)
            {
                return Task.FromResult<IActionResult>(BadRequest("Invalid email"));
            }
            if (!VerifyPasswordHash(request.Password, user.PasswordHash, user.PasswordSalt))
            {
                return Task.FromResult<IActionResult>(BadRequest("Invalid password"));
            }
            user.Token = CreateToken(user);
            user.UpdatedAt = DateTime.Now;
            _context.Users.Update(user);
            _context.SaveChanges();
            return Task.FromResult<IActionResult>(Ok(user));
        }

        private string CreateToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Email)
            };

            // Test if the token is in appsettings.json
            if (!string.IsNullOrEmpty(_configuration.GetSection("AppSettings:Token").Value))
            {
                string config = _configuration.GetSection("AppSettings:Token").Value ?? string.Empty;
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.Now.AddDays(1),
                    SigningCredentials = creds
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);

                return tokenHandler.WriteToken(token);
            }
            else
            {
                throw new Exception("Token not found in appsettings.json");
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computedHash.SequenceEqual(passwordHash);
            }
        }

        public bool IsAdmin(string email)
        {
            var user = _context.Users.FirstOrDefault(x => x.Email == email);
            if (user == null)
            {
                return false;
            }
            return user.IsAdmin;
        }
    }
}