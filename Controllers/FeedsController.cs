using api_rest.Context;
using Microsoft.AspNetCore.Mvc;

namespace api_rest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FeedController : ControllerBase
    {
        public static Feed feed = new Feed();
        private readonly ApiDbContext _context;

        public FeedController(ApiDbContext context)
        {
            _context = context;
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);
        }

        [HttpPost("create")]
        public IActionResult Create([FromBody] FeedDto request)
        {
            // check if the user is already a customer
            var customer = _context.Customers.FirstOrDefault(c => c.Id == request.CustomerId);
            if (customer == null || !customer.Active)
            {
                return BadRequest("User is not a customer");
            }
            if (!Uri.IsWellFormedUriString(request.Link, UriKind.Absolute))
            {
                return BadRequest("Link is not an url");
            }
            // create the feed
            feed = new Feed();
            feed.CustomerId = customer.Id;
            feed.Link = request.Link;
            // Save the feed in the database, if error return 500
            _context.Feeds.Add(feed);
            _context.SaveChanges();

            return Ok(feed);
        }

        [HttpPost("update/{id}")]
        public IActionResult Update(int id, [FromBody] FeedDto request)
        {
            // check if the user is already a customer
            var customer = _context.Customers.FirstOrDefault(c => c.Id == request.CustomerId);
            if (customer == null || !customer.Active)
            {
                return BadRequest("User is not a customer");
            }
            if (!Uri.IsWellFormedUriString(request.Link, UriKind.Absolute))
            {
                return BadRequest("Link is not an url");
            }
            // check if the feed exists
            var feed = _context.Feeds.FirstOrDefault(f => f.Id == id);
            if (feed == null)
            {
                return BadRequest("Feed does not exist");
            }
            // update the feed
            feed.CustomerId = customer.Id;
            feed.Link = request.Link;
            // Save the feed in the database, if error return 500
            _context.Feeds.Update(feed);
            _context.SaveChanges();

            return Ok(feed);
        }

        [HttpGet("get-all/{id}")]
        public IActionResult GetAll(int id)
        {
            // check if the user is already a customer
            var customer = _context.Customers.FirstOrDefault(c => c.Id == id);
            if (customer == null || !customer.Active)
            {
                return BadRequest("User is not a customer");
            }
            // get all the feeds
            var feeds = _context.Feeds.Where(f => f.CustomerId == customer.Id).ToList();
            return Ok(feeds);
        }

        [HttpDelete("delete/{id}")]
        public IActionResult Delete(int id)
        {
            // check if the feed exists
            var feed = _context.Feeds.FirstOrDefault(f => f.Id == id);
            if (feed == null)
            {
                return BadRequest("Feed does not exist");
            }
            // delete the feed
            _context.Feeds.Remove(feed);
            _context.SaveChanges();

            return Ok(feed);
        }
    }
}